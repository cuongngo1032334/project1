class Appointment < ActiveRecord::Base

  validates_presence_of :fee
  validates_numericality_of :fee, :greater_than_or_equal_to => 0
  belongs_to :patient
  belongs_to :specialist
end
