# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
insurances = Insurance.create([
                                  {name: 'Wellknown Insurance', street_address: '211 W Orem Dr Houston, TX 77047'},
                                  {name: 'No Worry', street_address: '1111 Shadow Creek Pkwy, Pearland, TX 77584'},
                                  {name: 'Cigna Health Group', street_address: '427 Fruge Rd, Houston, TX 77047'},
                                  {name: 'Wellcare Group', street_address: '14812 Hooper Rd, Houston, TX 77047'},
                                  {name: 'Blue Cross Blue Shield', street_address: '14109-14999 Hooper Rd, Houston, TX 77047'}
                              ])

patients = Patient.create([
                              {name: 'Maggie Q', street_address: '1700 Hadley St, Houston, TX 77003', insurance_id: 6},
                              {name: 'Megan Fox', street_address: '2500-2598 Chenevert St, Houston, TX 77004', insurance_id: 7},
                              {name: 'Scarlett Johansson', street_address: '1607 Drew St, Houston, TX 77004', insurance_id: 8},
                              {name: 'Charlize Theron', street_address: '2918 La Branch St, Houston, TX 77004', insurance_id: 9},
                              {name: 'Eva Mendes', street_address: '1304 Elgin St, Houston, TX 77004', insurance_id: 10}
                          ])

specialists = Specialist.create([
                                    {name: 'Teddy Bear', specialty: 'dermatologist'},
                                    {name: 'Mickey Mouse', specialty: 'endo dentist'},
                                    {name: 'Donald Duck', specialty: 'heart diseases'}
                                ])


appointments = Appointment.create([
                                      {specialist_id: 4, patient_id: 6, complaint: 'good', appointment_date: Date.today + 30, fee: 100.00},
                                      {specialist_id: 4, patient_id: 7, complaint: 'bad', appointment_date: Date.today + 25, fee: 400.32},
                                      {specialist_id: 5, patient_id: 8, complaint: 'normal', appointment_date: Date.today + 20, fee: 213.50},
                                      {specialist_id: 6, patient_id: 9, complaint: 'acceptable', appointment_date: Date.today + 19, fee: 224.22},
                                      {specialist_id: 6, patient_id: 10, complaint: 'slow', appointment_date: Date.today + 17, fee: 232.32},
                                      {specialist_id: 5, patient_id: 6, complaint: 'wait for 1 hour', appointment_date: Date.today + 16, fee: 546.21},
                                      {specialist_id: 4, patient_id: 9, complaint: 'overprice', appointment_date: Date.today + 17, fee: 453.34},
                                      {specialist_id: 5, patient_id: 7, complaint: 'careless doctor', appointment_date: Date.today + 18, fee: 45.43},
                                      {specialist_id: 6, patient_id: 8, complaint: 'n/a', appointment_date: Date.today + 17, fee: 34.23},
                                      {specialist_id: 5, patient_id: 10, complaint: 'ok', appointment_date: Date.today + 14, fee: 43.32},
                                      {specialist_id: 4, patient_id: 6, complaint: 'better now', appointment_date: Date.today + 15, fee: 234.12}
                                  ])